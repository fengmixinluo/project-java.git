package com.company;
import java.io.*;

public class WordCount {
    public static void main(String[] args) throws IOException {
        File f = new File(args[0]);
        try {
            if (f.exists()) {
                Lib.charnumber(f);
                Lib.wordsnumber(f);
                Lib.line(f);
                Lib.Wordsfreqret(f);

                int Number_of_characters = Lib.charnumber(f);
                int wordnumber = Lib.wordsnumber(f);
                int linenumber = Lib.line(f);
                String Wordsfreqret = Lib.Wordsfreqret(f);

                System.out.println(Number_of_characters);
                System.out.println(wordnumber);
                System.out.println(linenumber);
                System.out.println(Wordsfreqret);

                File output = new File("output.txt");
                if(!output.exists()){
                    output.createNewFile();
                }else {
                    PrintStream out1 = new PrintStream(output);
                    String out="characters:"+Number_of_characters+"\n"+"words:"+wordnumber+"\n"+"lines"+linenumber+"\n"+Wordsfreqret+"\n";
                    out1.print(out);
                    out1.close();
                }
            } else {
                System.out.println("文件不存在");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}